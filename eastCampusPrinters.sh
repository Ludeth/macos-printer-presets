#!/usr/bin/env bash

export PATH=/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/jamf/bin

pashuaLocation="/Library/Application Support/Cardinal"
ppdBasePath="/Library/Printers/PPDs/Contents/Resources/"
baseDownloadPath="https://gitlab.com/Ludeth/macos-printer-presets/raw/master/"
loggedInUser=`python -c 'from SystemConfiguration import SCDynamicStoreCopyConsoleUser; import sys; username = (SCDynamicStoreCopyConsoleUser(None, None, None) or [None])[0]; username = [username,""][username in [u"loginwindow", None, u""]]; sys.stdout.write(username + "\n");'`
plistBasePath="/Users/${loggedInUser}/Library/Preferences/com.apple.print.custompresets.plist"
plistIndividualBasePath="/Users/${loggedInUser}/Library/Preferences/com.apple.print.custompresets.forprinter."
setupLockedPrintScriptLocation="/Library/Application Support/Cardinal/setupPreset.py"
setupSpecificPrintScriptLocation="/Library/Application Support/Cardinal/setupPrinterSpecificPreset.py"
loggedInUserPid=$(python -c 'from SystemConfiguration import SCDynamicStoreCopyConsoleUser; username = SCDynamicStoreCopyConsoleUser(None, None, None)[1]; print(username);')
launchctlCmd=$(python -c 'import platform; from distutils.version import StrictVersion as SV; print("asuser") if SV(platform.mac_ver()[0]) >= SV("10.10") else "bsexec"')

if [ ! -e "${pashuaLocation}/pashua.sh" ]; then
  echo "Pashua does not exist. Installing"
  curl https://d1t6jrrr25jfsz.cloudfront.net/deployPashua.pkg > "${pashuaLocation}/deployPashua.pkg"
  installer -pkg "${pashuaLocation}/deployPashua.pkg" -target /
  rm "${pashuaLocation}/deployPashua.pkg"
fi

source "/Library/Application Support/Cardinal/pashua.sh"

windowConfigPrinterSelection="
*.title = South Campus Printers

printerSelection.type = popup
printerSelection.label = Please select a printer:
printerSelection.width = 310
printerSelection.option = East Campus 1B Printer 2 (Color)
printerSelection.option = South Campus 1st Floor Printer 3
printerSelection.option = South Campus 2nd Floor Printer 2

printerSelection.tooltip = Click to show printer list.

cancelButtonPrinter.type = cancelbutton
"

pashua_run "${windowConfigPrinterSelection}" "${pashuaLocation}"

if [ "${cancelButtonPrinter}" == "1" ]; then
  echo "User hit Cancel."
  exit 0
fi

case "${printerSelection}" in
  "East Campus 1B Printer 2 (Color)")
    PPDName="RICOH Aficio MP C2551"
    PPDFileDownloadName="RICOH_Aficio_MP_C2551.ppd"
    PrinterAddress="ipp://10.206.88.11/ipp/print"
    PrinterName="East_Campus_1B_Printer_2"
    PrinterDescription="East Campus 1B Printer 2 (Color)"
    PrinterIconPath="/Library/Printers/RICOH/Icons/331E.icns"
    PrinterIconDownloadName="331E.icn"
    PrinterOptions="printer-is-shared=false -o finisher=FinSAKAWAGAWA -o OptionTray=2Cassette"
    PrinterLocation="East Campus 1B"
    ;;
  "South Campus 1st Floor Printer 3")
    PPDName="RICOH Aficio MP 4002"
    PPDFileDownloadName="RICOH_Aficio_MP_4002.ppd"
    PrinterAddress="socket://10.206.88.13"
    PrinterName="South_Campus_1st_Floor_Printer_3"
    PrinterDescription="South Campus 1st Floor Printer 3 (B&W)"
    PrinterIconPath="/Library/Printers/RICOH/Icons/154E.icns"
    PrinterIconDownloadName="154E.icns"
    PrinterOptions="printer-is-shared=false -o finisher=FinKINGB -o OptionTray=2Cassette"
    PrinterLocation="South Campus 1st Floor Dock"
    ;;
  "South Campus 2nd Floor Printer 2")
    PPDName="RICOH MP C3503"
    PPDFileDownloadName="RICOH_MP_C3503.ppd"
    PrinterAddress="ipp://10.206.88.22/ipp/print"
    PrinterName="South_Campus_2nd_Floor_Printer_2"
    PrinterDescription="South Campus 2nd Floor Printer 2 (Color)"
    PrinterIconPath="/Library/Printers/RICOH/Icons/337E.icns"
    PrinterIconDownloadName="337E.icns"
    PrinterOptions="printer-is-shared=false -o finisher=FinRUBICONB -o OptionTray=2Cassette"
    PrinterLocation="South Campus 2nd Floor"
    ;;
esac

if [ ! -e "${PrinterIconPath}" ]; then
  curl "${baseDownloadPath}${PrinterIconDownloadName}" > "${PrinterIconPath}"
fi

if [ ! -e "${ppdBasePath}${PPDName}" ]; then
  curl "${baseDownloadPath}${PPDFileDownloadName}" > "${ppdBasePath}${PPDName}"
fi

lpadmin -p "${PrinterName}" -E -v "${PrinterAddress}" -P "${ppdBasePath}${PPDName}" -o "${PrinterOptions}" -D "${PrinterDescription}" -L "${PrinterLocation}"

plistKeyValue=$(/usr/libexec/PlistBuddy -c "Print ':Cardinal Locked Print:com.apple.print.preset.settings:com.ricoh.printsettings.11AUserID'" $plistBasePath >/dev/null 2>&1)
errorCode=($?)

if [ "${plistKeyValue}" == " " ]; then
  errorCode="1"
fi

if [ "${errorCode}" == "1" ]; then
  windowConfigLockCode="
  *.title = Setup Locked Print

  text.type = text
  text.default = Please note that on the Mac, you will set up your locked print credentials once and they will work with all Cardinal Health Ricoh printers. If you need to reset your locked print credentials, or just need to know what they are in the future, take a look at the corresponding jobs in Self Service.

  usernameBox.type = textfield
  usernameBox.label = Enter a printer username (8 character max):
  usernameBox.mandatory = Yes

  passwordBox.type = password
  passwordBox.label = Enter a 4 - 6 digit numerical PIN:
  passwordBox.mandatory = Yes

  cancelButtonPassword.type = cancelbutton
  "

  pashua_run "$windowConfigLockCode" "$pashuaLocation"

  lockedPrintUserName="${usernameBox}"
  lockedPrintPassword="${passwordBox}"

  curl "${baseDownloadPath}setupPreset.py" > "${setupLockedPrintScriptLocation}"
  curl "${baseDownloadPath}setupPrinterSpecificPreset.py" > "${setupSpecificPrintScriptLocation}"

  sudo -u ${loggedInUser} /usr/bin/python "${setupLockedPrintScriptLocation}" "${plistBasePath}" "${lockedPrintUserName}" "${lockedPrintPassword}"
  sudo -u ${loggedInUser} /usr/bin/python "${setupSpecificPrintScriptLocation}" "${plistIndividualBasePath}${PrinterName}.plist" "${lockedPrintUserName}" "${lockedPrintPassword}"
else
  curl "${baseDownloadPath}setupPreset.py" > "${setupLockedPrintScriptLocation}"
  curl "${baseDownloadPath}setupPrinterSpecificPreset.py" > "${setupSpecificPrintScriptLocation}"

  userIDKeyValue=$(/usr/libexec/PlistBuddy -c "Print ':Cardinal Locked Print:com.apple.print.preset.settings:com.ricoh.printsettings.11AUserID'" $plistBasePath >/dev/null 2>&1)
  passwordKeyValue=$(/usr/libexec/PlistBuddy -c "Print ':Cardinal Locked Print:com.apple.print.preset.settings:com.ricoh.printsettings.11APassword'" $plistBasePath >/dev/null 2>&1)
  sudo -u ${loggedInUser} /usr/bin/python "${setupSpecificPrintScriptLocation}" "${plistIndividualBasePath}${PrinterName}.plist" "${userIDKeyValue}" "${passwordKeyValue}"
fi

exit 0
