#!/usr/bin/python

import subprocess
import CoreFoundation
import socket
from SystemConfiguration import SCDynamicStoreCopyConsoleUser

def main():

    loggedInUser = username = (SCDynamicStoreCopyConsoleUser(None, None, None) or [None])[0]; username = [username,""][username in [u"loginwindow", None, u""]]

    plistPath = "/Users/%s/Library/Preferences/com.apple.print.custompresets.plist" % loggedInUser
    hostname = socket.gethostname()

    plistKeys = CoreFoundation.CFPreferencesCopyValue('Cardinal Locked Print', plistPath, loggedInUser, hostname)

    userName = plistKeys['com.apple.print.preset.settings']['com.ricoh.printsettings.11AUserID']
    password = plistKeys['com.apple.print.preset.settings']['com.ricoh.printsettings.11APassword']

    userInfo = "Username: %s\nPassword: %s" % (userName, password)

    template = '''display dialog "{}" with title "Locked Print Credentials" buttons "OK" default button "OK" with icon alias (("Macintosh HD:System:Library:CoreServices:CoreTypes.bundle:Contents:Resources:") & "LockedIcon.icns")'''

    subprocess.check_output(['osascript', '-e', template.format(userInfo)])

if __name__== "__main__":
    main()
