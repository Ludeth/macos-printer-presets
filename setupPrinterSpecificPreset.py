#!/usr/bin/python

import CoreFoundation
import sys

def setupLockedPrint(plistPath, username, password):
    CardinalLockedPrint = {
        "com.apple.print.preset.id": "com.apple.print.lastUsedSettingsPref",
        "com.apple.print.preset.settings": {
            "AP_D_InputSlot": "",
            "BookletBinding": 0,
            "CFPreferencesAppSynchronize": 2,
            "PaperInfoIsSuggested": 1,
            "com.apple.print.PageToPaperMappingAllowScalingUp": 1,
            "com.apple.print.PageToPaperMappingMediaName": "Letter",
            "com.apple.print.PageToPaperMappingType": 1,
            "com.apple.print.PrintSettings.PMColorSpaceModel": 3,
            "com.apple.print.PrintSettings.PMCopies": 1,
            "com.apple.print.PrintSettings.PMCopyCollate": 1,
            "com.apple.print.PrintSettings.PMDestinationType": 1,
            "com.apple.print.PrintSettings.PMDuplexing": 2,
            "com.apple.print.PrintSettings.PMFirstPage": 1,
            "com.apple.print.PrintSettings.PMLastPage": 2147483647,
            "com.apple.print.PrintSettings.PMPageRange": (1, 2147483647),
            "com.apple.print.PrintSettings.PMPrintSelectionOnly": 0,
            "com.apple.print.preset.Orientation": 1,
            "com.apple.print.preset.PaperInfo": {
                "paperInfo": {
                    "PMPPDPaperCodeName": "Letter",
                    "PMPPDTranslationStringPaperName": "US Letter",
                    "PMTiogaPaperName": "na-letter",
                    "com.apple.print.PageFormat.PMAdjustedPageRect": (0, 0, 768, 588),
                    "com.apple.print.PageFormat.PMAdjustedPaperRect": (-12, -12, 780, 600),
                    "com.apple.print.PaperInfo.PMCustomPaper": 0,
                    "com.apple.print.PaperInfo.PMPaperName": "na-letter",
                    "com.apple.print.PaperInfo.PMUnadjustedPageRect": (0, 0, 768, 588),
                    "com.apple.print.PaperInfo.PMUnadjustedPaperRect": (-12, -12, 780, 600),
                    "com.apple.print.PaperInfo.ppd.PMPaperName": "Letter",
                    "com.apple.print.ticket.type": "com.apple.print.PaperInfoTicket"
                },
            },
            "com.apple.print.subTicket.paper_info_ticket": {
                "PMPPDPaperCodeName": "Letter",
                "PMPPDTranslationStringPaperName": "US Letter",
                "PMTiogaPaperName": "na-letter",
                "com.apple.print.PaperInfo.PMDisplayName": "na-letter",
                "com.apple.print.PaperInfo.PMPPDPaperDimension": (0, 0, 612, 792),
                "com.apple.print.PaperInfo.PMPaperName": "na-letter",
                "com.apple.print.PaperInfo.PMUnadjustedPageRect": (0, 0, 768, 588),
                "com.apple.print.PaperInfo.PMUnadjustedPaperRect": (-12, -12, 780, 600),
                "com.apple.print.PaperInfo.ppd.PMPaperName": "Letter",
                "com.apple.print.ticket.type": "com.apple.print.PaperInfoTicket"
            },
            "com.apple.print.ticket.type": "com.apple.print.PrintSettingsTicket",
            "com.ricoh.printsettings.11AEnableSpecifyTime": 0,
            "com.ricoh.printsettings.11AEnableUserCode": 0,
            "com.ricoh.printsettings.11AFileName": "",
            "com.ricoh.printsettings.11AFolderNum": "",
            "com.ricoh.printsettings.11AFolderNumberSupport": True,
            "com.ricoh.printsettings.11AFolderPassword": "",
            "com.ricoh.printsettings.11AJobType": "Locked Print",
            "com.ricoh.printsettings.11APassword": "",
            "com.ricoh.printsettings.11ASpecifyTimeKeySupport": 1,
            "com.ricoh.printsettings.11ATimeZone": "EDT4",
            "com.ricoh.printsettings.11AUserCode": "",
            "com.ricoh.printsettings.11AUserID": "",
            "com.ricoh.printsettings.CCPCopyControlType": "Off",
            "com.ricoh.printsettings.CCPDensity": "Middle",
            "job-sheets": "none"
        },
    }
    CardinalLockedPrint["com.apple.print.preset.settings"]["com.ricoh.printsettings.11AUserID"] = username
    CardinalLockedPrint["com.apple.print.preset.settings"]["com.ricoh.printsettings.11APassword"] = password
    setAsDefault = "Cardinal Locked Print"
    setAsDefaultType = [5]

    CoreFoundation.CFPreferencesSetAppValue("com.apple.print.lastUsedSettingsPref", CardinalLockedPrint, plistPath)
    CoreFoundation.CFPreferencesSetAppValue("com.apple.print.lastPresetPrefType", setAsDefaultType, plistPath)
    CoreFoundation.CFPreferencesSetAppValue("com.apple.print.lastPresetPref", setAsDefault, plistPath)
    CoreFoundation.CFPreferencesAppSynchronize(plistPath)

def main():
    arguments = sys.argv
    plistPath = arguments[1]
    username = arguments[2]
    password = arguments[3]

    setupLockedPrint(plistPath, username, password)

if __name__ == '__main__':
    main()
