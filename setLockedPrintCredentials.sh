#!/usr/bin/env bash

export PATH=/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/jamf/bin

pashuaLocation="/Library/Application Support/Cardinal"
loggedInUser=`python -c 'from SystemConfiguration import SCDynamicStoreCopyConsoleUser; import sys; username = (SCDynamicStoreCopyConsoleUser(None, None, None) or [None])[0]; username = [username,""][username in [u"loginwindow", None, u""]]; sys.stdout.write(username + "\n");'`
plistBasePath="/Users/${loggedInUser}/Library/Preferences/com.apple.print.custompresets.plist"
setupLockedPrintScriptLocation="/Library/Application Support/Cardinal/setupPreset.py"

if [ ! -e "${pashuaLocation}/pashua.sh" ]; then
  echo "Pashua does not exist. Installing"
  curl https://d1t6jrrr25jfsz.cloudfront.net/deployPashua.pkg > "${pashuaLocation}/deployPashua.pkg"
  installer -pkg "${pashuaLocation}/deployPashua.pkg" -target /
  rm "${pashuaLocation}/deployPashua.pkg"
fi

source "/Library/Application Support/Cardinal/pashua.sh"

windowConfigLockCode="
*.title = Setup Locked Print

text.type = text
text.default = Please set your desired locked print credentials.

usernameBox.type = textfield
usernameBox.label = Enter a printer username (8 character max):
usernameBox.mandatory = Yes

passwordBox.type = password
passwordBox.label = Enter a 4 - 6 digit numerical PIN:
passwordBox.mandatory = Yes

cancelButtonPassword.type = cancelbutton
"
pashua_run "$windowConfigLockCode" "$pashuaLocation"

lockedPrintUserName="${usernameBox}"
lockedPrintPassword="${passwordBox}"

if [ ! -e "${setupLockedPrintScriptLocation}" ]; then
  curl https://storage.googleapis.com/cah-macos-ppds/setupPreset.py > "${setupLockedPrintScriptLocation}"
fi

sudo -u ${loggedInUser} /usr/bin/python "${setupLockedPrintScriptLocation}" "${plistBasePath}" "${lockedPrintUserName}" "${lockedPrintPassword}"

exit 0
