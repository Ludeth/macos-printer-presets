#!/usr/bin/env bash

#
# Script to add printers to macOS computers in the Cardinal Health environment
# this will work with and setup lock print on the printers in question
#

export PATH=/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/jamf/bin

##################################
#				 #
#	   VARIABLES		 #
#				 #
##################################
pashuaLocation="/Library/Application Support/Cardinal"
ppdBasePath="/Library/Printers/PPDs/Contents/Resources/"
baseDownloadPath="https://gitlab.com/Ludeth/macos-printer-presets/raw/master/"
baseIconDownload="https://gitlab.com/Ludeth/macos-printer-presets/blob/master/"
loggedInUser=`python -c 'from SystemConfiguration import SCDynamicStoreCopyConsoleUser; import sys; username = (SCDynamicStoreCopyConsoleUser(None, None, None) or [None])[0]; username = [username,""][username in [u"loginwindow", None, u""]]; sys.stdout.write(username + "\n");'`
plistBasePath="/Users/${loggedInUser}/Library/Preferences/com.apple.print.custompresets.plist"
plistIndividualBasePath="/Users/${loggedInUser}/Library/Preferences/com.apple.print.custompresets.forprinter."
setupLockedPrintScriptLocation="/Library/Application Support/Cardinal/setupPreset.py"
setupSpecificPrintScriptLocation="/Library/Application Support/Cardinal/setupPrinterSpecificPreset.py"
loggedInUserPid=$(python -c 'from SystemConfiguration import SCDynamicStoreCopyConsoleUser; username = SCDynamicStoreCopyConsoleUser(None, None, None)[1]; print(username);')
launchctlCmd=$(python -c 'import platform; from distutils.version import StrictVersion as SV; print("asuser") if SV(platform.mac_ver()[0]) >= SV("10.10") else "bsexec"')
baseIconPath="/Library/Printers/RICOH/Icons"

##################################
#                                #
#            MAIN	         #
#                                #
##################################

# We need to check to ensure that pashua is present to create popup dialog
# First we check for the folder
if [ ! -d "${pashuaLocation}" ]; then
  mkdir -p "${pashuaLocation}"
fi

# Check for the Pashua script and if it doesnt exist download and install it
if [ ! -e "${pashuaLocation}/pashua.sh" ]; then
  echo "Pashua does not exist. Installing"
  curl https://d1t6jrrr25jfsz.cloudfront.net/deployPashua.pkg > "${pashuaLocation}/deployPashua.pkg"
  installer -pkg "${pashuaLocation}/deployPashua.pkg" -target /
  rm "${pashuaLocation}/deployPashua.pkg"
fi

# Source this so we can use its functions and make Pashua work
source "/Library/Application Support/Cardinal/pashua.sh"

#
# This is the Pashua windo config
#
windowConfigPrinterSelection="
*.title = Mansfield Printers

printerSelection.type = popup
printerSelection.label = Please select a printer:
printerSelection.width = 310
printerSelection.option = Building 3 Floor 1 - RicPacking

printerSelection.tooltip = Click to show printer list.

cancelButtonPrinter.type = cancelbutton
"

# Lets run pashua and figure out what the user wants to install
pashua_run "${windowConfigPrinterSelection}" "${pashuaLocation}"

if [ "${cancelButtonPrinter}" == "1" ]; then
  echo "User hit Cancel."
  exit 0
fi

# Set a case for each of the printers at the site
case "${printerSelection}" in
  "Building 3 Floor 1 - RicPacking")
    PPDName="RICOH MP C5503"
    PPDFileDownloadName="RICOH_MP_C5503.ppd"
    PrinterAddress="ipp://172.24.12.30/ipp/print"
    PrinterName="Building_3_Floor_1_RicPacking"
    PrinterDescription="Building 3 Floor 1 - RicPacking"
    PrinterIconPath="/Library/Printers/RICOH/Icons/337E.icns"
    PrinterIconDownloadName="337E.icns"
    PrinterOptions="printer-is-shared=false -o finisher=FinSAKAWAGAWA -o OptionTray=2Cassette"
    PrinterLocation="Building 3 Floor 1"
    ;;
esac

# Check to see if the icon path folder exists and create it if needed
if [ ! -d "${baseIconPath}" ]; then
  mkdir -p "${baseIconPath}"
fi

# Download the printer icon so everything looks nice
if [ ! -e "${PrinterIconPath}" ]; then
  curl "${baseDownloadPath}${PrinterIconDownloadName}" > "${PrinterIconPath}"
fi

# Download the PPD file so that the printer actually works
if [ ! -e "${ppdBasePath}${PPDName}" ]; then
  curl "${baseDownloadPath}${PPDFileDownloadName}" > "${ppdBasePath}${PPDName}"

fi

# This command installs the printer and options set out above in the switch case
lpadmin -p "${PrinterName}" -E -v "${PrinterAddress}" -P "${ppdBasePath}${PPDName}" -o "${PrinterOptions}" -D "${PrinterDescription}" -L "${PrinterLocation}"

# We need to check if Locked Print is already setup. We can use PlistBuddy to check for the presence of a key
plistKeyValue=$(/usr/libexec/PlistBuddy -c "Print ':Cardinal Locked Print:com.apple.print.preset.settings:com.ricoh.printsettings.11AUserID'" $plistBasePath >/dev/null 2>&1)
errorCode=($?)

if [ "${plistKeyValue}" == " " ]; then
  errorCode="1"
fi

# If locked print was not setup lets set it up!
if [ "${errorCode}" == "1" ]; then
  # Some more Pashua config
  windowConfigLockCode="
  *.title = Setup Locked Print

  text.type = text
  text.default = Please note that on the Mac, you will set up your locked print credentials once and they will work with all Cardinal Health Ricoh printers. If you need to reset your locked print credentials, or just need to know what they are in the future, take a look at the corresponding jobs in Self Service.

  usernameBox.type = textfield
  usernameBox.label = Enter a printer username (8 character max):
  usernameBox.mandatory = Yes

  passwordBox.type = password
  passwordBox.label = Enter a 4 - 6 digit numerical PIN:
  passwordBox.mandatory = Yes

  cancelButtonPassword.type = cancelbutton
  "
  # Run the lock print pashua window
  pashua_run "$windowConfigLockCode" "$pashuaLocation"
  
  # Store users name and password
  lockedPrintUserName="${usernameBox}"
  lockedPrintPassword="${passwordBox}"
  
  # Grab the latest versions of the locked print setup scripts and printer preset setup scripts
  curl "${baseDownloadPath}setupPreset.py" > "${setupLockedPrintScriptLocation}"
  curl "${baseDownloadPath}setupPrinterSpecificPreset.py" > "${setupSpecificPrintScriptLocation}"
  
  # Run the python to setup the lock print and printer preseets
  sudo -u ${loggedInUser} /usr/bin/python "${setupLockedPrintScriptLocation}" "${plistBasePath}" "${lockedPrintUserName}" "${lockedPrintPassword}"
  sudo -u ${loggedInUser} /usr/bin/python "${setupSpecificPrintScriptLocation}" "${plistIndividualBasePath}${PrinterName}.plist" "${lockedPrintUserName}" "${lockedPrintPassword}"
else
  curl "${baseDownloadPath}setupPreset.py" > "${setupLockedPrintScriptLocation}"
  curl "${baseDownloadPath}setupPrinterSpecificPreset.py" > "${setupSpecificPrintScriptLocation}"

  userIDKeyValue=$(/usr/libexec/PlistBuddy -c "Print ':Cardinal Locked Print:com.apple.print.preset.settings:com.ricoh.printsettings.11AUserID'" $plistBasePath >/dev/null 2>&1)
  passwordKeyValue=$(/usr/libexec/PlistBuddy -c "Print ':Cardinal Locked Print:com.apple.print.preset.settings:com.ricoh.printsettings.11APassword'" $plistBasePath >/dev/null 2>&1)
  sudo -u ${loggedInUser} /usr/bin/python "${setupSpecificPrintScriptLocation}" "${plistIndividualBasePath}${PrinterName}.plist" "${userIDKeyValue}" "${passwordKeyValue}"
fi

exit 0
